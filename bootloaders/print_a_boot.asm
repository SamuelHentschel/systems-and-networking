.code16                         #tell the assembler that we're using 16-bits 
                                #(real mode)

.global _start                  #make the _start label globally seen
_start:                         #beginning of the binary
        mov $0x0e41, %ax        #set ah to 0xe (telemetry) and the character
                                #to print to 0x41 (A)
        int $0x10               #call the function in ah with 10h interrupt
        hlt

.org 510                        #fill the rest of the space with zeros
.word 0xAA55                    #magic bytes for bootloading
