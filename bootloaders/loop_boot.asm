.code16                         #tell the assembler that we're using 16-bits 
                                #(real mode)

.global _start                  #make the _start label globally seen
_start:                         #beginning of the binary
        jmp _start

.org 510                        #fill the rest of the space with zeros
.word 0xAA55                    #magic bytes for bootloading
