.code16                         #tell the assembler that we're using 16-bits 
                                #(real mode)

.global _start                  #make the _start label globally seen
_start:                         #beginning of the binary
        mov $0x0e, %ah          #sets ah to 0xe (teletype)
        mov $msg, %si           #sets si to the address of the first byte
print_char:
        lodsb                   #loads byte from address in si into al and
                                #increments si
        cmp $0, %al             #compares character with null character
        je done                 #jump to the end if the character is null
        int $0x10               #call the function in ah with 10h interrupt
        jmp print_char
done:
        hlt

msg: .asciz "Hello World!"     #stores the string with a nullbyte

.org 510                        #fill the rest of the space with zeros
.word 0xAA55                    #magic bytes for bootloading
